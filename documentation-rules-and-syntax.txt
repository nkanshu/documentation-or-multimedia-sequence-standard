****everything is in an xml context.

Root element is a 'document' (within quotes) tag.

'before' and 'after' are 2 attributes (within quotes) that tell which document comes before and after the current document during navigation. Valid value can be a relative path (with respect to the file with the before and after declarations) to document xml files.

possible child of document tag can be 1 or more than one content tags.
(plus 'attributes' (within quotes) tag at the beginning, only once)
...
content element and its child elements: <title></title> (only once) and <body></body> only once.

Only character text inside body and title tags has the potential to be a part of the document text.

**body tag can have multiple content tags as children.**

body can also have following children based on content-types:

content-type            tag
list                    list
table                   table
multi-media             multi
...

Any tag can have attributes tag as first child.
all the children tags (along with their character text) of attributes will serve as the different attributes and values for the parent tag of the attributes tag. In case of a redundancy of attributes between xml attributes and attributes within the attributes tag, the values declared in attributes tag will override the official xml attributes.
...

list, table, multi tags can have leadcaption and trailcaption children tags at the beginning (just after attributes tag) and end respectively. They only have content tags as possible children tag.
...

There is an 'import' tag (within quotes).

The 'import' tag (within quotes) has a contenttype attribute. It has possible values of 'txt' (within quotes), 'table' (within quotes), 'list' (within quotes), 'multi' (within quotes), 'xml' (within quotes) and 'others' (within quotes). It points to import content from other files. The character text of this tag gives the relative path of the file to import content from. The path is relative to the file containing the import declaration.

any tag can have an import tag as a child.
...

list tag can have an attribute type with possible values 'numbered' and 'unnumbered'.

For numbered list another possible attribute will be start.
It will have 4 compulsory children with compulsory values, to define the sequence for the numbers - startone, starttwo, startthree and startfour.
default sequence will be 1, 2, 3, 4 ...

list tag will have li as a child that represents each item in the list. It can have 1 or more than 1 content tags.
...

stub
    resources
        animated-images
            gif
                keep your gif file(s) here
        audio
            aiff
                keep your aiff file(s) here
            au
                keep your au file(s) here
            mp3
                keep your mp3 file(s) here
            rmf
                keep your rmf file(s) here
            smf
                keep your smf file(s) here
            wav
                keep your wav file(s) here
        documents
            doc
                keep your doc file(s) here
            docx
                keep your docx file(s) here
            epub
                keep your epub file(s) here
            fodt
                keep your fodt file(s) here
            odt
                keep your odt file(s) here
            pdf
                keep your pdf file(s) here
            rtf
                keep your rtf file(s) here
        images
            fodg
                keep your fodg file(s) here
            jpg
                keep your jp(e)g file(s) here
            odg
                keep your odg file(s) here
            png
                keep your png file(s) here
            svg
                keep your svg file(s) here
            tif
                keep your tif(f) file(s) here
        movies
            3gp
                keep your 3gp(p) file(s) here
            avi
                keep your avi file(s) here
            mkv
                keep your mkv file(s) here
            mov
                keep your mov file(s) here
            mp4
                keep your mp4 file(s) here
            ogg
                keep your ogg file(s) here
        presentations
            fodp
                keep your fodp file(s) here
            odp
                keep your odp file(s) here
            ppt
                keep your ppt file(s) here
            pptx
                keep your pptx file(s) here
        spreadsheets
            fods
                keep your fods file(s) here
            ods
                keep your ods file(s) here
            xls
                keep your xls file(s) here
            xlsx
                keep your xlsx file(s) here
    src
        main
            c
                keep your c file(s) here
            css
                keep your css file(s) here
            html
                keep your htm(l) file(s) here
            java
                keep your java file(s) here
            js
                keep your js file(s) here
            json
                keep your json file(s) here
            txt
                keep your txt file(s) here
            xml
                keep your xml file(s) here
        test
            c
                keep your test c file(s) here
            css
                keep your test css file(s) here
            html
                keep your test htm(l) file(s) here
            java
                keep your test java file(s) here
            js
                keep your test js file(s) here
            json
                keep your test json file(s) here
            txt
                keep your test txt file(s) here
            xml
                keep your test xml file(s) here
